package src;

import java.util.*;

/** Quaternions. Basic operations. */
public class Quaternion {

	/** double numbers less than DELTA are considered zero */
	public static final double DELTA = 0.000001;

	private double r, i, j, k;
	// TODO!!! Your fields here!

	/**
	 * Constructor from four double values.
	 * 
	 * @param a
	 *            real part
	 * @param b
	 *            imaginary part i
	 * @param c
	 *            imaginary part j
	 * @param d
	 *            imaginary part k
	 */
	public Quaternion(double a, double b, double c, double d) {
		r = a;
		i = b;
		j = c;
		k = d;
	}

	/**
	 * Real part of the quaternion.
	 * 
	 * @return real part
	 */
	public double getRpart() {
		return r; // TODO!!!
	}

	/**
	 * Imaginary part i of the quaternion.
	 * 
	 * @return imaginary part i
	 */
	public double getIpart() {
		return i; // TODO!!!
	}

	/**
	 * Imaginary part j of the quaternion.
	 * 
	 * @return imaginary part j
	 */
	public double getJpart() {
		return j; // TODO!!!
	}

	/**
	 * Imaginary part k of the quaternion.
	 * 
	 * @return imaginary part k
	 */
	public double getKpart() {
		return k; // TODO!!!
	}

	/**
	 * Conversion of the quaternion to the string.
	 * 
	 * @return a string form of this quaternion: "a+bi+cj+dk" (without any
	 *         brackets)
	 */
	@Override
	public String toString() {
		return r + "+" + i + "i+" + j + "j+" + k + "k"; // TODO!!!
	}

	/**
	 * Conversion from the string to the quaternion. Reverse to
	 * <code>toString</code> method.
	 * 
	 * @throws IllegalArgumentException
	 *             if string s does not represent a quaternion (defined by the
	 *             <code>toString</code> method)
	 * @param s
	 *            string of form produced by the <code>toString</code> method
	 * @return a quaternion represented by string s
	 */
	public static Quaternion valueOf(String s) {
		double z1, z2, z3, z4;
		String [] arr = s.trim().split("\\++");
		System.out.println(Arrays.toString(arr));
		for (int i = 0; i < arr.length; i++) {
			if(arr[i].contains("i") || arr[i].contains("j") || arr[i].contains("i") || arr[i].contains("k")){
				arr[i] = arr[i].replace(arr[i].substring(arr[i].length()-1), "");
			}
		}
		System.out.println(Arrays.toString(arr));
		try {
			
			if(arr.length == 4){
				z1 = Double.parseDouble(arr[0]);
				z2 = Double.parseDouble(arr[1]);
				z3 = Double.parseDouble(arr[2]);
				z4 = Double.parseDouble(arr[3]);
				return new Quaternion(z1, z2, z3, z4);
			}
			else {
				throw new IllegalArgumentException("String " + s + " is not quaternion.");
			}
		} catch (Exception e) {
			throw new IllegalArgumentException("Parsing failed");
		}
		
	}

	/**
	 * Clone of the quaternion.
	 * 
	 * @return independent clone of <code>this</code>
	 */
	@Override
	public Object clone() throws CloneNotSupportedException {
		
		return new Quaternion(this.r, this.i, this.j, this.k); // TODO!!!
	}

	/**
	 * Test whether the quaternion is zero.
	 * 
	 * @return true, if the real part and all the imaginary parts are (close to)
	 *         zero
	 */
	public boolean isZero() {

		return this.equals(new Quaternion(0., 0., 0., 0.)); // TODO!!!
	}

	/**
	 * Conjugate of the quaternion. Expressed by the formula
	 * conjugate(a+bi+cj+dk) = a-bi-cj-dk
	 * 
	 * @return conjugate of <code>this</code>
	 */
	public Quaternion conjugate() {
		return new Quaternion(r, -i, -j, -k); // TODO!!!
	}

	/**
	 * Opposite of the quaternion. Expressed by the formula opposite(a+bi+cj+dk)
	 * = -a-bi-cj-dk
	 * 
	 * @return quaternion <code>-this</code>
	 */
	public Quaternion opposite() {
		return new Quaternion(-r, -i, -j, -k); // TODO!!!
	}

	/**
	 * Sum of quaternions. Expressed by the formula (a1+b1i+c1j+d1k) +
	 * (a2+b2i+c2j+d2k) = (a1+a2) + (b1+b2)i + (c1+c2)j + (d1+d2)k
	 * 
	 * @param q
	 *            addend
	 * @return quaternion <code>this+q</code>
	 */
	public Quaternion plus(Quaternion q) {
		Quaternion x = this;
		return new Quaternion(x.r + q.r, x.i + q.i, x.j + q.j, x.k + q.k); // TODO!!!
	}

	/**
	 * Product of quaternions. Expressed by the formula (a1+b1i+c1j+d1k) *
	 * (a2+b2i+c2j+d2k) = (a1a2-b1b2-c1c2-d1d2) + (a1b2+b1a2+c1d2-d1c2)i +
	 * (a1c2-b1d2+c1a2+d1b2)j + (a1d2+b1c2-c1b2+d1a2)k
	 * 
	 * @param q
	 *            factor
	 * @return quaternion <code>this*q</code>
	 */
	public Quaternion times(Quaternion q) {
		Quaternion x = this;
		double y1, y2, y3, y4;
		y1 = x.r * q.r - x.i * q.i - x.j * q.j - x.k * q.k;
		y2 = x.r * q.i + x.i * q.r + x.j * q.k - x.k * q.j;
		y3 = x.r * q.j - x.i * q.k + x.j * q.r + x.k * q.i;
		y4 = x.r * q.k + x.i * q.j - x.j * q.i + x.k * q.r;
		return new Quaternion(y1, y2, y3, y4); // TODO!!!
	}

	/**
	 * Multiplication by a coefficient.
	 * 
	 * @param r
	 *            coefficient
	 * @return quaternion <code>this*r</code>
	 */
	public Quaternion times(double r) {
		return new Quaternion(this.r * r, this.i * r, this.j * r, this.k * r); // TODO!!!
	}

	/**
	 * Inverse of the quaternion. Expressed by the formula 1/(a+bi+cj+dk) =
	 * a/(a*a+b*b+c*c+d*d) + ((-b)/(a*a+b*b+c*c+d*d))i +
	 * ((-c)/(a*a+b*b+c*c+d*d))j + ((-d)/(a*a+b*b+c*c+d*d))k
	 * 
	 * @return quaternion <code>1/this</code>
	 */
	public Quaternion inverse() {
		if(this.equals(new Quaternion(0.,0.,0.,0.))){
			throw new RuntimeException("Ei saa jagada nulliga!");
		}
		double a = (r * r + i * i + j * j + k * k);
		return new Quaternion(r / a, -i / a, -j / a, -k / a); // TODO!!!
	}

	/**
	 * Difference of quaternions. Expressed as addition to the opposite.
	 * 
	 * @param q
	 *            subtrahend
	 * @return quaternion <code>this-q</code>
	 */
	public Quaternion minus(Quaternion q) {
		return new Quaternion(this.r - q.r, this.i - q.i, this.j - q.j, this.k - q.k); // TODO!!!
	}

	/**
	 * Right quotient of quaternions. Expressed as multiplication to the
	 * inverse.
	 * 
	 * @param q
	 *            (right) divisor
	 * @return quaternion <code>this*inverse(q)</code>
	 */
	public Quaternion divideByRight(Quaternion q) {
		if (q.isZero()) {
			throw new RuntimeException("Ei saa jagada nulliga!");
		}
		return this.times(q.inverse()); // TODO!!!
	}

	/**
	 * Left quotient of quaternions.
	 * 
	 * @param q
	 *            (left) divisor
	 * @return quaternion <code>inverse(q)*this</code>
	 */
	public Quaternion divideByLeft(Quaternion q) {
		if(q.isZero()){
			throw new RuntimeException("Ei saa jagada nulliga!");
		}
		return q.inverse().times(this); // TODO!!!
	}

	/**
	 * Equality test of quaternions. Difference of equal numbers is (close to)
	 * zero.
	 * 
	 * @param qo
	 *            second quaternion
	 * @return logical value of the expression <code>this.equals(qo)</code>
	 */
	@Override
	public boolean equals(Object qo) {
		Quaternion quat = (Quaternion) qo;
		if (Math.abs(this.r - quat.r) < DELTA && Math.abs(this.i - quat.i) < DELTA && 
			Math.abs(this.j - quat.j) < DELTA && Math.abs(this.k - quat.k) < DELTA) {
			return true;
		}
		return false; // TODO!!!
	}

	/**
	 * Dot product of quaternions. (p*conjugate(q) + q*conjugate(p))/2
	 * 
	 * @param q
	 *            factor
	 * @return dot product of this and q
	 */
	public Quaternion dotMult(Quaternion q) {
		return this.times(q.conjugate()).plus(q.times(this.conjugate())).times(0.5); // TODO!!!
	}

	/**
	 * Integer hashCode has to be the same for equal objects.
	 * Meetod saadi:
	 * http://docs.ros.org/diamondback/api/comp_germandeli/html/java/Quaternion_8java_source.html
	 * @return hashcode
	 */
	@Override
	public int hashCode() {
		int prime = 31;
		int result = 1;
		long tmp;
		result = prime * result + (int)((tmp = Double.doubleToLongBits(this.r)) ^ (tmp >>> 32));
		result = prime * result + (int)((tmp = Double.doubleToLongBits(this.i)) ^ (tmp >>> 32));
		result = prime * result + (int)((tmp = Double.doubleToLongBits(this.j)) ^ (tmp >>> 32));
		result = prime * result + (int)((tmp = Double.doubleToLongBits(this.k)) ^ (tmp >>> 32));
		return result;
		
		 // TODO!!!
	}

	/**
	 * Norm of the quaternion. Expressed by the formula norm(a+bi+cj+dk) =
	 * Math.sqrt(a*a+b*b+c*c+d*d)
	 * 
	 * @return norm of <code>this</code> (norm is a real number)
	 */
	public double norm() {
		return Math.sqrt(this.r*this.r + this.i*this.i + this.j*this.j + this.k*this.k); // TODO!!!
	}

	/**
	 * Main method for testing purposes.
	 * 
	 * @param arg
	 *            command line parameters
	 */
	public static void main(String[] arg) {
		Quaternion arv1 = new Quaternion(-1., 1, 2., -2.);
		if (arg.length > 0)
			arv1 = valueOf(arg[0]);
		System.out.println("first: " + arv1.toString());
		System.out.println("real: " + arv1.getRpart());
		System.out.println("imagi: " + arv1.getIpart());
		System.out.println("imagj: " + arv1.getJpart());
		System.out.println("imagk: " + arv1.getKpart());
		System.out.println("isZero: " + arv1.isZero());
		System.out.println("conjugate: " + arv1.conjugate());
		System.out.println("opposite: " + arv1.opposite());
		System.out.println("hashCode: " + arv1.hashCode());
		Quaternion res = null;
		try {
			res = (Quaternion) arv1.clone();
		} catch (CloneNotSupportedException e) {
		}
		;
		System.out.println("clone equals to original: " + res.equals(arv1));
		System.out.println("clone is not the same object: " + (res != arv1));
		System.out.println("hashCode: " + res.hashCode());
		res = valueOf(arv1.toString());
		System.out.println("string conversion equals to original: " + res.equals(arv1));
		Quaternion arv2 = new Quaternion(1., -2., -1., 2.);
		if (arg.length > 1)
			arv2 = valueOf(arg[1]);
		System.out.println("second: " + arv2.toString());
		System.out.println("hashCode: " + arv2.hashCode());
		System.out.println("equals: " + arv1.equals(arv2));
		res = arv1.plus(arv2);
		System.out.println("plus: " + res);
		System.out.println("times: " + arv1.times(arv2));
		System.out.println("minus: " + arv1.minus(arv2));
		double mm = arv1.norm();
		System.out.println("norm: " + mm);
		System.out.println("inverse: " + arv1.inverse());
		System.out.println("divideByRight: " + arv1.divideByRight(arv2));
		System.out.println("divideByLeft: " + arv1.divideByLeft(arv2));
		System.out.println("dotMult: " + arv1.dotMult(arv2));
	}
}
// end of file
